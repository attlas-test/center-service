document.addEventListener('DOMContentLoaded', () => {
    const { store, bus, emitter, event, util, storage } = ori

    store.origin.watch()
    emitter.click()
    emitter.keyboard()

    if (window.user) {
        store.g_username = user.username
    }

    event.on('g_user_logout', () => {
        util.setCookie('token', '', -1)
        location.href = '/login'
    })
    
    event.on('copy_me', (_, { target }) => {
        event.emit('copy', target.innerText)
    })

    event.on('show_me', (_, { target }) => {
        event.emit('simple_modal_show', target.innerHTML)
    })

    event.on('main_include_ui_content_done',  () => {
        if (!document.title) {
            const uiTitle = document.querySelector('.content-header span')?.innerText
            const uiDesciption = document.querySelector('.content-header small')?.innerText

            if (uiTitle) {
                document.title = `${uiTitle} | ${uiDesciption}`
            }
        }
    })

    const activeMenu = (dom) => {
        let at = dom
            || document.querySelector(`.sidebar a[href='${location.pathname}${location.search}']`)
            || document.querySelector(`.sidebar a[href='${location.pathname}']`)

        if (at) {
            let parentMenu = at.parentElement

            while (parentMenu && parentMenu.className !== 'sidebar') {
                at.tagName === 'LI' && at.addClass('active menu-open')
                at = parentMenu
                parentMenu = at.parentElement
            }
        }
    }

    window.sidebar_toggle.addEventListener('click', (e) => {
        if (document.body.className.indexOf('sidebar-collapse') !== -1) {
            localStorage.is_sidebar_open = '1'
        } else {
            localStorage.is_sidebar_open = ''
        }  
    })

    window.sidebar_menu.addEventListener('click', (e) => {
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return
        }

        if (e.target.className.includes('menu-i')) {
            document.querySelector('.treeview.active')?.removeClass('active')
            document.querySelector('.menu-open.active')?.removeClass('active')
            activeMenu(e.target)
        }
    })

    activeMenu()

    if (!storage.uuid) {
        storage.uuid = util.uuid()
    }

    bus.post('/user/v1/devices', {
        uuid: storage.uuid,
        name: storage.uuid,
        location: {
            accuracy: 2630.984233247639,
            latitude: 21.0277644,
            longitude: 105.8341598,
        }
    }).catch(console.error)
})