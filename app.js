const { create, gateway } = require('eroc')
const vanguard = require('./vanguard')

create(async (app) => {
    app.get('/login', (req, res, next) => {
        return res.render('login', { layout: false })
    })

    app.get('/register', (req, res, next) => {
        return res.render('register', { layout: false })
    })

    app.get('/', vanguard.gate({ page: true }), (req, res, next) => {
        return res.render('home')
    })

    app.get('/:service([a-z-]+)~:path(*+)', vanguard.gate({ page: true }), (req, res, next) => {
        return res.render('mf')
    })

    app.use('/:service([a-z-]+)/ui/*', vanguard.gate())

    app.use(vanguard.gate({ weak: true }), gateway())
})
