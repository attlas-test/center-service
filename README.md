# Hướng dẫn cài đặt

Em đã thử cài đặt trước trên môi trường linux tại: http://103.74.123.247:5000

Ứng dụng gồm 2 databse và 2 microservice, vì thời gian không cho phép nên em 
gộp hết chức năng vào service user-service

Vì dựng 1 cụm nhỏ nên thay vì dùng centralized configuration em để hết config 
tại file config.js

Ứng dụng khi start sẽ bắt đầu crawl các block của ETH để lấy thông tin 
blockNumber-timestamp. Mỗi 5p sẽ start crawl lại nếu bị dừng

Em đã crawl sẵn tại db 'mongodb://shovity:ytivohs@103.74.123.247/a-user'
anh có thể dùng luôn hoặc import lại

Toàn bộ source code được public tại gitlab group: https://gitlab.com/attlas-test

## Prepare

```
- docker
- docker-compose
- yarn (for run scripts)
```

## Setup database

Redis
```bash
git clone https://gitlab.com/attlas-test/redis-database.git
cd redis-database
docker-compose up -d
```

Mongodb
```bash
git clone https://gitlab.com/attlas-test/mongo-database.git
cd mongo-database
docker-compose up -d
```

## Setup application

Center
```bash
git clone https://gitlab.com/attlas-test/center-service.git
cd center-service
yarn up prod

# Develop with hot reload
yarn
yarn up local
```

User
```bash
git clone https://gitlab.com/attlas-test/user-service.git
cd user-service
yarn up prod

# Develop with hot reload
yarn
yarn up local
```

```
Ứng dụng chạy tại cổng `5000`
```

# Note 

Những vấn đề em chưa ưng ý và chưa làm được

### Chức năng 1

- Thiếu xác thực tài khoản bằng sms/email

- Thiếu chức năng open authentication (không hỗ trợ non-secure protocols)

- Thiếu thao tác nhấn "enter" để thực hiện đăng ký/đăng nhập   

### Chức năng 2

- Không lấy được thông tin location (không hỗ trợ non-secure protocols)

- Không lấy được tên thiệt bị

### Chức năng 3

- Danh sách transfer transaction không phân trang được do không tính trước 
được tổng số log transfer trong 30 ngày

- Chưa áp dụng được các ký thuật caching. Mặc dù dữ liệu blockchain có đặc 
thù không thay đổi, chỉ append rất phù hợp để tích hợp cache

- Các request đến node chưa cần auth nhưng vẫn phải thực hiện ở server.
Phần này có thể đẩy xử lý về phía client tránh làm quá tải server và rate 
limit của node

- Phụ thuộc vào shared node nên không ổn định

### Chức năng 4

- IU/UX tệ