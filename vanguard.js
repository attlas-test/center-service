const { redis } = require('eroc')

const vanguard = {}

vanguard.gate = (option = {}) => {
    return (req, res, next) => {
        const handle = async () => {
            if (!req.u.user) {
                if (option.weak) {
                    return next()
                }

                if (option.page) {
                    return res.redirect(`/login?next=${req.originalUrl}`)
                } else {
                    return res.status(401).error('401 Unauthorized')
                }
            }

            if (req.u.user) {
                const tiat = await redis.hget('device_tiat', req.u.user.device)

                if (tiat === null || req.u.user.iat < tiat) {
                    res.u.cookie('token', '')

                    if (option.page) {
                        return res.redirect(`/login?next=${req.originalUrl}`)
                    } else {
                        return res.status(401).error('You logged-out')
                    }
                }
            }

            next()
        }

        handle().catch((error) => {
            res.u.cookie('token', '')
            return next(error)
        })
    }
}

module.exports = vanguard
