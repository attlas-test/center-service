#!/usr/bin/env bash

# Usage:
# sh bin/up [local, prod]

env=$1

case "$env" in
  local)
    docker-compose -f docker-compose.yml -f docker-compose.local.yml up
    ;;

  prod)
    docker-compose -f docker-compose.yml up -d
    ;;

  *)
    echo "Environment not found! please choose [prod, local]"
esac